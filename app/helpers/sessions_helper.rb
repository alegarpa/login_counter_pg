module SessionsHelper

  # Logs in the given user.
  def log_in(user)
    session[:user_id] = user.id
    user.login_count += 1
    user.save
  end

  def log_out
  	session.delete(:user_id)
  	@current_user = nil
  end

  # Returns the current logged-in user (if any).

  #this should be updated for cookies lol
  def current_user
    #user == current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    else
    end
  end

  def current_user?(user)
    user == current_user
  end

  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
  
  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end

  #def update_count
  #	@current_user ||= User.find_by(id: session[:user_id])
  #	@current_user.login_count += 1
  #	@current_user.save
  #end
end
