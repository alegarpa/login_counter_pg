class Payment 
	attr_reader :amount_cents, :amount_currency
	
	def initialize
		@amount_cents = 0
		@amount_currency = 'USD'
	end

	def amount
		Money.new amount_cents, amount_currency
	end

	def amount=(amount_value)
		raise ArgumentError unless amount_value.respond_to? :to_money
		money = amount_value.to_money
		@amount_cents = money.cents
		@amount_currency = money.currency_as_string
		money
	end



end