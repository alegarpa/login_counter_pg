require 'test_helper'

class PaymentTest < ActiveSupport::TestCase

	def setup 
		@payment = Payment.new
		@fifteen_dollars = Money.new '1500', 'USD'
	end

	def test_string
		@payment.amount = '15'
		assert_equal @fifteen_dollars, @payment.amount
	end
	def test_string_with_symbol
    	@payment.amount = '$15'
    	assert_equal @fifteen_dollars, @payment.amount
  	end

  	def test_string_decimal
    	@payment.amount = '15.00'
    	assert_equal @fifteen_dollars, @payment.amount
  	end

  	def test_string_decimal_with_symbol
    	@payment.amount = '$15.00'
    	assert_equal @fifteen_dollars, @payment.amount
  	end

  	def test_integer
    	@payment.amount = 15
   		assert_equal @fifteen_dollars, @payment.amount
  	end

 	 def test_decimal
    	@payment.amount = 15.00
    	assert_equal @fifteen_dollars, @payment.amount
  	end






end
